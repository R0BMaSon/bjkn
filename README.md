# TEAM BJKN README - Management Server #

## Before you run the Management Server ansible-playbook! ##

- This ansible-playbook is designed to build the user an ec2-instance that is provisioned with jenkins, creation of a S3 bucket and creation of a vault server!

- Firstly you need to change the keyname parameter to your amazon key name in the all file. This is found under the group_vars folder.

- Next you must remove the DNS name from under the jenkins host. This is located in the group_vars folder in the file hosts.

## Once you're ready to run the ansible-playbook!

- Now you are ready to run the playbook!!
- Now you can go to your chosen terminal and cd into the bjkn directory.
- First run this command to generate and retrieve your new key pair `ansible-playbook -i environment/prod playbook.yml`. This is your very own key pair which gives you access to the remove servers.
- Then run this command `ansible-playbook -i environment/prod site.yml`
- This will create all of the management systems.
- once the playbook has been completed, you can log on to the Jenkins server by grabbing the public dns name that is displayed in the terminal.
- Once you have coppied DNS name of the jenkins server, paste this in your chosen browser with :8080 after the DNS name.

## Ready to delete?!? ##
- once you're ready to delete jenkins, vault and the s3 bucket you just have to run this command!
- `ansible-playbook -i environment/prod remove.yml`
- This will delete everything created by this git repo - so make sure you really want to do this
